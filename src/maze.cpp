#include <iostream>
#include <fstream>

#include "maze.h"

void Maze::break_vertical_wall(int k1, int k2){
    assert(std::abs(k1 - k2) == dim);
    if(k1 < k2){
        MazeCell *left_cell = get_cell(k1);
        left_cell->set_right_wall(false);
        MazeCell *right_cell = get_cell(k2);
        right_cell->set_left_wall(false);
    }
    else if(k1 > k2){
        MazeCell *right_cell = get_cell(k1);
        right_cell->set_left_wall(false);
        MazeCell *left_cell = get_cell(k2);
        left_cell->set_right_wall(false);
    }
    return;
}

void Maze::break_vertical_wall(int row, int col1, int col2){
    break_vertical_wall(row + col1*dim, row + col2*dim);
    return;
}

void Maze::break_horizontal_wall(int k1, int k2){
    assert(std::abs(k1 - k2) == 1);
    if(k1 < k2){
        MazeCell *top_cell = get_cell(k1);
        top_cell->set_bottom_wall(false);
        MazeCell *bottom_cell = get_cell(k2);
        bottom_cell->set_top_wall(false);
    }
    else if(k1 > k2){
        MazeCell *bottom_cell = get_cell(k1);
        bottom_cell->set_top_wall(false);
        MazeCell *top_cell = get_cell(k2);
        top_cell->set_bottom_wall(false);
    }
    return;
}

void Maze::break_horizontal_wall(int col, int row1, int row2){
    break_horizontal_wall(row1 + col*dim, row2 + col*dim);
    return;
}

void draw_mazecell(sf::RenderWindow *window, const MazeCell *cell, float x, float y){
    sf::RectangleShape wall(sf::Vector2f(WALL_THICKNESS, WALL_LENGTH));
    wall.setFillColor(sf::Color::Black);

    if(cell->has_left_wall()){
        wall.setPosition(x - WALL_THICKNESS/2, y - WALL_THICKNESS/2);
        window->draw(wall);
    }
    
    if(cell->has_right_wall()){
        wall.setPosition(x + CELL_SIZE - WALL_THICKNESS/2, y - WALL_THICKNESS/2);
        window->draw(wall);
    }
    
    wall.setSize(sf::Vector2f(WALL_LENGTH, WALL_THICKNESS));
    
    if(cell->has_top_wall()){
        wall.setPosition(x - WALL_THICKNESS/2, y - WALL_THICKNESS/2);
        window->draw(wall);
    }

    if(cell->has_bottom_wall()){
        wall.setPosition(x - WALL_THICKNESS/2, y + CELL_SIZE - WALL_THICKNESS/2);
        window->draw(wall);
    }

    return;
}

void draw_maze(sf::RenderWindow *window, const Maze *maze, float x, float y){
    int n = maze->get_dim(), nn = n*n;
    for(int k = 0; k < nn; k++){
        int row = k%n, col = k/n;
        float x0 = x + col*CELL_SIZE, y0 = y + row*CELL_SIZE;
        draw_mazecell(window, maze->get_cell(k), x0, y0);
    }
    return;
}

void maze_to_dot(Maze *maze, const std::string &filename){
    std::ofstream file(filename, std::ios_base::out);
    if(file){
        file << "graph M {\n";
        int n = maze->get_dim(), nn = n*n;
        for(int k = 0; k < nn; k++){
            int row = k%n, col = k/n;
            int r1 = row+1, c1 = col, k1 = r1 + c1*n; // cell below
            if(maze->is_valid_cell(r1, c1) &&
              !maze->get_cell(k)->has_bottom_wall() &&
              !maze->get_cell(k1)->has_top_wall()){
                file << k << "--" << k1 << "\n";
                maze->get_cell(k)->set_bottom_wall(true);
                maze->get_cell(k1)->set_top_wall(true);
            }
            
            r1 = row-1; c1 = col; k1 = r1 + c1*n; // cell above 
            if(maze->is_valid_cell(r1, c1) &&
              !maze->get_cell(k)->has_top_wall() &&
              !maze->get_cell(k1)->has_bottom_wall()){
                file << k << "--" << k1 << "\n";
                maze->get_cell(k)->set_top_wall(true);
                maze->get_cell(k1)->set_bottom_wall(true);
            }
            
            r1 = row; c1 = col+1; k1 = r1 + c1*n; // right cell
            if(maze->is_valid_cell(r1, c1) &&
              !maze->get_cell(k)->has_right_wall() &&
              !maze->get_cell(k1)->has_left_wall()){
                file << k << "--" << k1 << "\n";
                maze->get_cell(k)->set_right_wall(true);
                maze->get_cell(k1)->set_left_wall(true);
            }
            
            r1 = row; c1 = col-1; k1 = r1 + c1*n; // left cell
            if(maze->is_valid_cell(r1, c1) &&
              !maze->get_cell(k)->has_left_wall() &&
              !maze->get_cell(k1)->has_right_wall()){
                file << k << "--" << k1 << "\n";
                maze->get_cell(k)->set_left_wall(true);
                maze->get_cell(k1)->set_right_wall(true);
            }
        }
        file << "}";
    }
    else std::cout << "Cannot open file " << filename << " for writing\n";
    return;
}
