#include <queue>

#include "maze.h"

void bfs(int s, int t, const Maze *maze, std::unordered_map<int,int> *parents){
    int n = maze->get_dim();
    std::queue<int> to_visit; to_visit.push(s); parents->operator[](s) = -1;

    while(!to_visit.empty()){
        int k = to_visit.front(); to_visit.pop(); // pop top cell to visit
        if(k == t) return;
        int row = k%n, col = k/n;
        int r1 = row+1, c1 = col, k1 = r1 + c1*n; // cell below
        if(maze->is_valid_cell(r1, c1) && parents->find(k1) == parents->end() &&
          !maze->get_cell(k)->has_bottom_wall() &&
          !maze->get_cell(k1)->has_top_wall()){
            to_visit.push(k1); parents->operator[](k1) = k;
        }
        
        r1 = row-1; c1 = col; k1 = r1 + c1*n; // cell above 
        if(maze->is_valid_cell(r1, c1) && parents->find(k1) == parents->end() &&
          !maze->get_cell(k)->has_top_wall() &&
          !maze->get_cell(k1)->has_bottom_wall()){
            to_visit.push(k1); parents->operator[](k1) = k;
        }
        
        r1 = row; c1 = col+1; k1 = r1 + c1*n; // right cell
        if(maze->is_valid_cell(r1, c1) && parents->find(k1) == parents->end() &&
          !maze->get_cell(k)->has_right_wall() &&
          !maze->get_cell(k1)->has_left_wall()){
            to_visit.push(k1); parents->operator[](k1) = k;
        }
        
        r1 = row; c1 = col-1; k1 = r1 + c1*n; // left cell
        if(maze->is_valid_cell(r1, c1) && parents->find(k1) == parents->end() &&
          !maze->get_cell(k)->has_left_wall() &&
          !maze->get_cell(k1)->has_right_wall()){
            to_visit.push(k1); parents->operator[](k1) = k;
        }
    }
    return;
}

int get_path(int s, int t, const std::unordered_map<int,int> *parents, std::vector<int> *path){
    while(parents->at(t) != -1){
        path->push_back(t); t = parents->at(t);
    }
    if(t != s){
        printf("path is NOT correct\n"); return -1;
    }
    else{
        path->push_back(t); return path->size();
    }
}

void draw_path(sf::RenderWindow *window, const std::vector<int> *path, float x, float y, int n){
    int len = path->size(), len1 = len-1;
    for(int i = 0; i < len1; i++){
        int v1 = path->at(i  ), r1 = v1%n, c1 = v1/n;
        int v2 = path->at(i+1), r2 = v2%n, c2 = v2/n;

        float x1 = x + c1*CELL_SIZE + CELL_SIZE/2, y1 = y + r1*CELL_SIZE + CELL_SIZE/2;
        float x2 = x + c2*CELL_SIZE + CELL_SIZE/2, y2 = y + r2*CELL_SIZE + CELL_SIZE/2;

        sf::Vertex line[2] = {sf::Vertex(sf::Vector2f(x1, y1), sf::Color::Red),
                              sf::Vertex(sf::Vector2f(x2, y2), sf::Color::Red)};
        
        window->draw(line, 2,sf::Lines);
    }
    return;
}
