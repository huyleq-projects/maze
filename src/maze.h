#ifndef MAZE_H
#define MAZE_H

#define CELL_SIZE 10.f
#define WALL_THICKNESS 2.f
#define WALL_LENGTH (CELL_SIZE + WALL_THICKNESS)

#include <cmath>
#include <cassert>
#include <vector>
#include <unordered_map>
#include <string>

#include <SFML/Graphics.hpp>

class MazeCell{
    public:
    MazeCell(): walls(0xf){}
    
    bool has_left_wall()   const { return walls & 0x1;}
    bool has_right_wall()  const { return walls & 0x2;}
    bool has_top_wall()    const { return walls & 0x4;}
    bool has_bottom_wall() const { return walls & 0x8;}
    
    void set_left_wall(bool w){ 
        if(w) walls |= 0x1; else walls &= 0xe; return;
    }
    
    void set_right_wall(bool w){ 
        if(w) walls |= 0x2; else walls &= 0xd; return;
    }
    
    void set_top_wall(bool w){ 
        if(w) walls |= 0x4; else walls &= 0xb; return;
    }
    
    void set_bottom_wall(bool w){ 
        if(w) walls |= 0x8; else walls &= 0x7; return;
    }

    private:
    char walls;
};

class Maze{
    public:
    Maze(int n): cells(n*n), dim(n){}
    
    bool is_valid_cell(int row, int col) const {
        return (row >= 0 && row < dim && col >= 0 &&col < dim);
    }
    
    bool is_valid_cell(int k) const { 
        int row = k%dim, col = k/dim;
        return is_valid_cell(row, col);
    }
    
    MazeCell *get_cell(int row, int col){
        assert(is_valid_cell(row, col));
        return cells.data() + dim*col + row;
    }

    const MazeCell *get_cell(int row, int col) const{
        assert(is_valid_cell(row, col));
        return cells.data() + dim*col + row;
    }
    
    MazeCell *get_cell(int k){
        assert(is_valid_cell(k)); return cells.data() + k;
    }

    const MazeCell *get_cell(int k) const{
        assert(is_valid_cell(k)); return cells.data() + k;
    }

    int get_dim() const { return dim;}

    void break_vertical_wall(int k1, int k2);
    void break_vertical_wall(int row, int col1, int col2);
    void break_horizontal_wall(int k1, int k2);
    void break_horizontal_wall(int col, int row1, int row2);

    private:
    std::vector<MazeCell> cells;
    int dim;
};

void draw_mazecell(sf::RenderWindow *window, const MazeCell *cell, float x, float y);
void draw_maze(sf::RenderWindow *window, const Maze *maze, float x, float y);
void maze_to_dot(Maze *maze, const std::string &filename);

void build_maze_recursive_backtrack(Maze *maze);
void build_maze_kruskal(Maze *maze);
void build_maze_prim(Maze *maze);
void build_maze_euler(Maze *maze);

void bfs(int s, int t, const Maze *maze, std::unordered_map<int,int> *parents);
int get_path(int s, int t, const std::unordered_map<int,int> *parents, std::vector<int> *path);
void draw_path(sf::RenderWindow *window, const std::vector<int> *path, float x, float y, int n);

#endif
