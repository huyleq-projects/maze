#ifndef FOREST_H
#define FOREST_H

#include <vector>
#include <string>

struct ForestNode{
    public:
    ForestNode(): parent(-1), rank(-1){}
    ForestNode(int p): parent(p), rank(0){}
    ForestNode(int p, int r): parent(p), rank(r){}

    int parent, rank;
};

class Forest{
    public:
    Forest(int n): nodes(n), size(n){ init(); return;}
    
    int find(int k);
    bool is_connected(int x, int y);
    void connect(int x, int y);
    void to_dot(const std::string &filename);

    private:
    void init();
    void link(int x, int y);

    std::vector<ForestNode> nodes;
    int size;
};

int find_connected_components(std::vector<std::vector<int>> &cc,
                              Forest &forest,
                              const std::vector<int> &nodes);

#endif
