#include <cassert>
#include <iostream>
#include <fstream>

#include "forest.h"

void Forest::init(){
    for(int i = 0; i < size; i++){
        nodes[i].parent = i; nodes[i].rank = 0; 
    }
    return;
}

int Forest::find(int k){
    assert((k >= 0 && k < size));
    int p = nodes[k].parent;
    if(p != k) nodes[k].parent = find(p);
    return nodes[k].parent;
}

bool Forest::is_connected(int x, int y){
    return (find(x) == find(y));
}

void Forest::link(int x, int y){
    if(nodes[x].rank > nodes[y].rank) nodes[y].parent = x;
    else{
        nodes[x].parent = y;
        if(nodes[x].rank == nodes[y].rank) nodes[y].rank++;
    }
    return;
}

void Forest::connect(int x, int y){
    link(find(x), find(y));
    return;
}

void Forest::to_dot(const std::string &filename){
    std::ofstream file(filename, std::ios_base::out);
    if(file){
        file << "digraph M {\n";
        for(int i = 0; i < size; i++) file << i << " -> " << nodes[i].parent << "\n";
        file << "}";
    }
    else std::cout << "Cannot open file " << filename << " for writing\n";
    return;
}

int find_connected_components(std::vector<std::vector<int>> &cc,
                              Forest &forest,
                              const std::vector<int> &nodes){
    for(int x: nodes){
        bool found = false;
        for(std::vector<int> &component: cc){
            if(!component.empty() && forest.is_connected(x, component[0])){
                component.push_back(x); found = true; break;
            }
        }
        if(!found) cc.emplace_back(std::vector<int>{x});
    }
    return cc.size();
}
