#include <unordered_set>
#include <random>
#include <chrono>
#include <algorithm>

#include "maze.h"
#include "forest.h"

int carve_wall(int k, Maze *maze, const std::unordered_set<int> *visited){
    std::vector<std::pair<int,int>> walllist;
    int n = maze->get_dim(), row = k%n, col = k/n;
    int r1 = row+1, c1 = col, next = r1 + c1*n; // cell below
    if(maze->is_valid_cell(r1, c1) && visited->find(next) == visited->end() &&
       maze->get_cell(k)->has_bottom_wall() &&
       maze->get_cell(next)->has_top_wall()) walllist.emplace_back(r1, c1);
    
    r1 = row-1; c1 = col; next = r1 + c1*n; // cell above
    if(maze->is_valid_cell(r1, c1) && visited->find(next) == visited->end() &&
       maze->get_cell(k)->has_top_wall() &&
       maze->get_cell(next)->has_bottom_wall()) walllist.emplace_back(r1, c1);
    
    r1 = row; c1 = col+1; next = r1 + c1*n; // right cell
    if(maze->is_valid_cell(r1, c1) && visited->find(next) == visited->end() &&
       maze->get_cell(k)->has_right_wall() &&
       maze->get_cell(next)->has_left_wall()) walllist.emplace_back(r1, c1);
    
    r1 = row; c1 = col-1; next = r1 + c1*n; // left cell
    if(maze->is_valid_cell(r1, c1) && visited->find(next) == visited->end() &&
       maze->get_cell(k)->has_left_wall() &&
       maze->get_cell(next)->has_right_wall()) walllist.emplace_back(r1, c1);
    
    int m = walllist.size();
    if(m == 0) return -1;
    else{ // pick a random wall to carve
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine generator(seed);
        std::uniform_int_distribution<int> distribution(0,m-1);
        int i = distribution(generator);
        r1 = walllist[i].first; c1 = walllist[i].second; next = r1 + c1*n;
        if(row == r1) maze->break_vertical_wall(k, next);
        else if(col == c1) maze->break_horizontal_wall(k, next);
        return next;
    }
}

void recursive_backtrack_helper(int k, Maze *maze, std::unordered_set<int> *visited){
    visited->insert(k);
    int next;
    while((next = carve_wall(k, maze, visited)) > 0)
        recursive_backtrack_helper(next, maze, visited);
    return;
}

void build_maze_recursive_backtrack(Maze *maze){
    int n = maze->get_dim();
    std::unordered_set<int> visited;
    
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(0,n*n-1);
    int k = distribution(generator);

    recursive_backtrack_helper(k, maze, &visited);
    return;
}

void build_maze_kruskal(Maze *maze){
    int n = maze->get_dim(), nn = n*n, n1 = n-1, n1n = n1*n;
    
    std::vector<int> vwalls(n1n), hwalls(n1n);
    for(int i = 0; i < n1n; i++) vwalls[i] = hwalls[i] = i;

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::shuffle(vwalls.begin(), vwalls.end(), generator);
    std::shuffle(hwalls.begin(), hwalls.end(), generator);
    
    std::uniform_real_distribution<float> distribution(0, 1);
    Forest forest(nn);

    int i = 0, j = 0, k1, k2;
    while(i < n1n && j < n1n){
        float r = distribution(generator);
        if(r < 0.5f){ // choose a vertical wall
            int k = vwalls[i++], row = k%n, col1 = k/n, col2 = col1 + 1;
            k1 = row + col1*n; k2 = row + col2*n;
            if(!forest.is_connected(k1, k2)){
                maze->break_vertical_wall(k1, k2);
                forest.connect(k1, k2); 
            }
        }
        else{ // choose a horizontal wall
            int k = hwalls[j++], row1 = k%n1, row2 = row1 + 1, col = k/n1;
            k1 = row1 + col*n; k2 = row2 + col*n;
            if(!forest.is_connected(k1, k2)){
                maze->break_horizontal_wall(k1, k2);
                forest.connect(k1, k2); 
            }
        }
    }
    
    while(i < n1n){
        int k = vwalls[i++], row = k%n, col1 = k/n, col2 = col1 + 1;
        k1 = row + col1*n; k2 = row + col2*n;
        if(!forest.is_connected(k1, k2)){
            maze->break_vertical_wall(k1, k2);
            forest.connect(k1, k2); 
        }
    }

    while(j < n1n){
        int k = hwalls[j++], row1 = k%n1, row2 = row1 + 1, col = k/n1;
        k1 = row1 + col*n; k2 = row2 + col*n;
        if(!forest.is_connected(k1, k2)){
            maze->break_horizontal_wall(k1, k2);
            forest.connect(k1, k2); 
        }
    }
    return;
}

int pick_random_wall(std::unordered_set<int> *walls){
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(0, walls->size()-1);

    int k = distribution(generator);
    std::unordered_set<int>::iterator it = walls->begin();
    std::advance(it, k); 
    int w = *it;
    walls->erase(it);
    return w;
}

void add_walls(int k, const Maze *maze, std::unordered_set<int> *visited,
                                       std::unordered_set<int> *vwalls,
                                       std::unordered_set<int> *hwalls){
    const MazeCell *cell = maze->get_cell(k);
    int n = maze->get_dim(), n1 = n-1, row = k%n, col = k/n;
    
    int row1 = row, col1 = col-1; // left cell 
    if(cell->has_left_wall() && maze->is_valid_cell(row1, col1)){ // left wall
        int k1 = row1 + col1*n, w = k1; // in this case wall id = k1
        if(visited->find(k1) == visited->end()) vwalls->insert(w);
        else vwalls->erase(w);
    }
    
    row1 = row; col1 = col+1; // right cell 
    if(cell->has_right_wall() && maze->is_valid_cell(row1, col1)){ // right wall
        int k1 = row1 + col1*n, w = k; // in this case wall id = k
        if(visited->find(k1) == visited->end()) vwalls->insert(w);
        else vwalls->erase(w);
    }
    
    row1 = row-1; col1 = col; // cell above
    if(cell->has_top_wall() && maze->is_valid_cell(row1, col1)){ // top wall
        int k1 = row1 + col1*n, w = row1 + n1*col1;
        if(visited->find(k1) == visited->end()) hwalls->insert(w);
        else hwalls->erase(w);
    }
    
    row1 = row+1; col1 = col; // cell below
    if(cell->has_bottom_wall() && maze->is_valid_cell(row1, col1)){ // bottom wall
        int k1 = row1 + col1*n, w = row + n1*col;
        if(visited->find(k1) == visited->end()) hwalls->insert(w);
        else hwalls->erase(w);
    }
    
    visited->insert(k);
    return;
}

void build_maze_prim(Maze *maze){
    int n = maze->get_dim(), n1 = n-1, nn = n*n;
    
    std::unordered_set<int> visited, vwalls, hwalls;
    
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<float> distribution(0,1);
    
    std::uniform_int_distribution<int> distribution1(0,n*n-1);
    int k = distribution1(generator);
    add_walls(k, maze, &visited, &vwalls, &hwalls);

    while(visited.size() < nn){
        int k1, k2;
        float r = distribution(generator);
        
        if(r < 0.5f){ // pick a vertical wall
            int w = pick_random_wall(&vwalls);
            int row = w%n, col1 = w/n, col2 = col1 + 1;
            k1 = row + col1*n; k2 = row + col2*n;
            maze->break_vertical_wall(k1, k2);
        }
        else{ // pick a horizontal wall
            int w = pick_random_wall(&hwalls);
            int row1 = w%n1, row2 = row1 + 1, col = w/n1;
            k1 = row1 + col*n; k2 = row2 + col*n;
            maze->break_horizontal_wall(k1, k2);
        }

        if(visited.find(k1) == visited.end()) add_walls(k1, maze, &visited, &vwalls, &hwalls);
        else add_walls(k2, maze, &visited, &vwalls, &hwalls);
    }
    return;
}

void build_maze_euler(Maze *maze){
    int n = maze->get_dim(), nn = n*n, n1 = n-1;
    Forest forest(nn);
    std::vector<int> x(n);

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<float> distribution(0,1);
    
    for(int col = 0; col < n1; col++){
        for(int row = 0; row < n1; row++){ // randomly carve walls between unconnected cells
            int row1 = row + 1, k = row + col*n, k1 =  row1 + col*n;
            if(!forest.is_connected(k, k1) && distribution(generator) >= 0.5){
                maze->break_horizontal_wall(k, k1);
                forest.connect(k, k1);
            }
        }

        for(int row = 0; row < n; row++) x[row] = row + col*n;
        std::vector<std::vector<int>> cc;
        find_connected_components(cc, forest, x);

        for(std::vector<int> &component: cc){
            shuffle(component.begin(), component.end(), generator);
            int k = component[0], k1 = k + n;
            maze->break_vertical_wall(k, k1); // break at least 1 vertical call per component
            forest.connect(k, k1);
            for(int i = 1; i < component.size(); i++){
                if(distribution(generator) >= 0.5){
                    k = component[i]; k1 = k + n;
                    maze->break_vertical_wall(k, k1);
                    forest.connect(k, k1);
                }
            }
        }
    }

    // last column, break walls between unconnected cells
    int col = n1;
    for(int row = 0; row < n1; row++){
        int row1 = row + 1, k = row + n1*n, k1 = row1 + n1*n;
        if(!forest.is_connected(k, k1)){
            maze->break_horizontal_wall(k, k1);
            forest.connect(k, k1);
        }
    }
    return;
}
