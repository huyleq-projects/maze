#include "maze.h"

int main(){
    int n = 5;
    
    float window_width = 2*n*CELL_SIZE, window_height = window_width;
    sf::RenderWindow window(sf::VideoMode(window_width, window_height),"Maze");
    window.clear(sf::Color::White);
    
    Maze maze(n);
    maze.break_vertical_wall(1, 2, 3);
    maze.break_horizontal_wall(1, 2, 3);
    float x = window_width/4, y = window_height/4;

    while(window.isOpen()){
        sf::Event event;
        while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed ||
              (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Q))
                window.close();
        }

        window.clear(sf::Color::White);
        draw_maze(&window, &maze, x, y);
        window.display();
    }
    
    return 0;
}
