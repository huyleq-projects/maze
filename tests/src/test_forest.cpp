#include "forest.h"

int main(){
    Forest forest(7);
    forest.connect(5, 1);
    forest.connect(2, 6);
    forest.connect(1, 6);
    forest.connect(4, 0);
    forest.connect(3, 4);
//    forest.connect(3, 1);
    
    if(forest.is_connected(5, 4)) printf("5 and 4 are connected\n");
    printf("5 and 4 are NOT connected\n");

    forest.to_dot("forest.dot");
    
    std::vector<int> x{0, 1, 2, 3, 4, 5, 6};
    std::vector<std::vector<int>> cc;
    int ncc = find_connected_components(cc, forest, x);
    printf("number of cc: %d\n", ncc);
    
    for(const std::vector<int> &component: cc){
        for(int v: component) printf("%d ", v);
        printf("\n");
    }
    return 0;
}
