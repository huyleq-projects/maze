#include <cstdio>
#include <cstdlib>
#include <iostream>

#include "maze.h"

int main(int argc, char **argv){
    if(argc < 4){
        printf("Choose 3 integers for maze size, building method, and solving method\n");
        printf("./bin/test_buildmaze.x size bmethod smethod\n");
        printf("bmethod: 0 -> recursive backtrack\n");
        printf("         1 -> kruskal\n");
        printf("         2 -> prim\n");
        printf("         3 -> euler\n");
        printf("smethod: 0 -> BFS\n");
        return 0;
    }

    int n = atoi(argv[1]), bmethod = atoi(argv[2]), smethod = atoi(argv[3]);
    Maze maze(n);

    switch(bmethod){
        case 0:
        printf("building maze by recursive backtracksort\n");
        build_maze_recursive_backtrack(&maze);
        break;

        case 1:
        printf("building maze by kruskal\n");
        build_maze_kruskal(&maze);
        break;

        case 2:
        printf("building maze by prim\n");
        build_maze_prim(&maze);
        break;

        case 3:
        printf("building maze by euler\n");
        build_maze_euler(&maze);
        break;

        default:
        printf("building maze by recursive backtracksort\n");
        build_maze_recursive_backtrack(&maze);
    }
    
    int s = 0, t = n*n-1, len = 0;
    std::unordered_map<int,int> parents;
    std::vector<int> path;
    switch(smethod){
        case 0:
        printf("solving maze by BFS\n");
        bfs(s, t, &maze, &parents);
        len = get_path(s, t, &parents, &path);
        printf("path length: %d\n", len);
        break;
        
        default:
        printf("Not solving maze\n");
    }

    float window_width = (n+2)*CELL_SIZE, window_height = window_width;
    sf::RenderWindow window(sf::VideoMode(window_width, window_height),"Maze");
    window.clear(sf::Color::White);
    
    float x = CELL_SIZE, y = CELL_SIZE;

    while(window.isOpen()){
        sf::Event event;
        while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed ||
              (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Q))
                window.close();
        }

        window.clear(sf::Color::White);
        draw_maze(&window, &maze, x, y);
        if(len > 0) draw_path(&window, &path, x, y, n);
        window.display();
    }

    maze_to_dot(&maze, "maze.dot");
    return 0;
}
